<?php declare(strict_types=1);

namespace SubbkovOpenSource\TestMyVendor;

class FibonacciException extends \Exception
{
    /**
     * FibonacciException constructor.
     *
     * @param string $message
     */
    public function __construct(string $message = '')
    {
        parent::__construct('Fibonacci Error: ' . $message);
    }
}
