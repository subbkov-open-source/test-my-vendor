<?php declare(strict_types=1);

namespace SubbkovOpenSource\TestMyVendor\Commands;

use SubbkovOpenSource\TestMyVendor\Fibonacci;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FibonacciCommand extends Command
{
    /** @var Fibonacci */
    private $fibonacci;

    /**
     * FibonacciCommand constructor.
     *
     * @param Fibonacci $fibonacci
     */
    public function __construct(Fibonacci $fibonacci)
    {
        $this->fibonacci = $fibonacci;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('fibonacci')
            ->setDescription('Get fibonacci number')
            ->addArgument('number');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $number = $input->getArgument('number');
            $output->writeln($number . ' in a row Fibonacci number: ' . $this->fibonacci->run($number));
        } catch (\Throwable $exception) {
            $output->writeln($exception->getMessage());
        }
    }
}
