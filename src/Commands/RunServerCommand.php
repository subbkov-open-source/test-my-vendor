<?php declare(strict_types=1);

namespace SubbkovOpenSource\TestMyVendor\Commands;

use SubbkovOpenSource\TestMyVendor\Fibonacci;
use SubbkovOpenSource\TestMyVendor\RunServerException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RunServerCommand extends Command
{
    public const HOST = '127.0.0.1';

    /** @var resource */
    private $socket;

    /** @var int */
    private $port;

    private $welcomeMessage = "\nWelcome to the PHP server to check the Fibonacci number.\n";

    private $helpMessage =
        "\nServer Commands:\n" .
        "\t'fibonacci' - calculates the number of fibonacci\n" .
        "\t'help' - command assistance\n" .
        "\t'exit' - disconnect from server\n" .
        "\t'kill' - turn off the server\n";

    private $separator = "---------------------------------------------------------\n";

    /** @var Fibonacci */
    private $fibonacci;

    public function __construct(Fibonacci $fibonacci)
    {
        $this->fibonacci = $fibonacci;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('run-server')
            ->setDescription('Run PHP server websocket')
            ->addArgument('port');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->port = (int) $input->getArgument('port');

            if (0 === $this->port) {
                throw new RunServerException('Incorrectly specified server port ' . $this->port);
            }

            $this->runServer();
        } catch (\Throwable $exception) {
            $output->writeln($exception->getMessage());
        }
    }

    /**
     * @throws RunServerException
     */
    private function runServer(): void
    {
        \set_time_limit(0);

        \ob_implicit_flush();

        $this->createSocket();

        do {
            if (false === ($socketMessage = \socket_accept($this->socket))) {
                throw new RunServerException(
                    'Failed to do socket_accept():',
                    $this->socket
                );
            }

            $msg = $this->welcomeMessage . $this->helpMessage . $this->separator;
            \socket_write($socketMessage, $msg, \strlen($msg));

            do {
                if (false === ($buf = \socket_read($socketMessage, 2048, PHP_NORMAL_READ))) {
                    throw new RunServerException(
                        'Failed to do socket_read():',
                        $this->socket,
                        $socketMessage
                    );
                }

                $msgReturn = '';

                if (!$buf = \trim($buf)) {
                    continue;
                }
                if ($buf === 'help') {
                    $msgReturn = $this->helpMessage;
                }

                if ($buf === 'exit') {
                    break;
                }
                if ($buf === 'kill') {
                    \socket_close($socketMessage);
                    break 2;
                }

                if (\preg_match('/fibonacci (.*)/', $buf, $res)) {
                    /** @var string $number */
                    $number = (string) $res[1];
                    try {
                        $msgReturn = 'result: ' . $this->fibonacci->run($number);
                    } catch (\Throwable $exception) {
                        $msgReturn = $exception->getMessage();
                    }
                }

                if (!empty($msgReturn)) {
                    $msgReturn .= "\n";
                }
                \socket_write($socketMessage, $msgReturn, strlen($msgReturn));
            } while (true);
            \socket_close($socketMessage);
        } while (true);

        \socket_close($this->socket);
    }

    /**
     * @throws RunServerException
     */
    private function createSocket(): void
    {
        if (false === ($this->socket = \socket_create(AF_INET, SOCK_STREAM, SOL_TCP))) {
            throw new RunServerException(
                'Failed to do socket_create():',
                $this->socket
            );
        }

        if (false === \socket_bind($this->socket, self::HOST, $this->port)) {
            throw new RunServerException(
                'Failed to do socket_bind():',
                $this->socket
            );
        }

        if (false === \socket_listen($this->socket, 5)) {
            throw new RunServerException(
                'Failed to do socket_listen():',
                $this->socket
            );
        }
    }
}
