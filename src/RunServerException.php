<?php declare(strict_types=1);

namespace SubbkovOpenSource\TestMyVendor;

class RunServerException extends \Exception
{
    /**
     * RunServerException constructor.
     *
     * @param string $message
     * @param mixed  $socket
     * @param mixed  $socketChild
     */
    public function __construct(string $message = '', $socket = null, $socketChild = null)
    {
        $reasonMessage = '';

        if (null !== $socketChild) {
            socket_close($socketChild);
        }

        if (null !== $socket) {
            $reasonMessage = ' Reason: ' . socket_strerror(socket_last_error($socket));
            socket_close($socket);
        }

        parent::__construct('Run PHP server websocket Error: ' . $message . $reasonMessage);
    }
}
