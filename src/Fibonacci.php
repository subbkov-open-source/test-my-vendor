<?php declare(strict_types=1);

namespace SubbkovOpenSource\TestMyVendor;

use SubbkovOpenSource\Fibonacci\Fibonacci as VendorFibonacci;

class Fibonacci
{
    /** @var VendorFibonacci */
    private $fibonacci;

    /**
     * Fibonacci constructor.
     *
     * @param VendorFibonacci $fibonacci
     */
    public function __construct(VendorFibonacci $fibonacci)
    {
        $this->fibonacci = $fibonacci;
    }

    /**
     * @param string|null $number
     *
     * @throws FibonacciException
     *
     * @return int
     */
    public function run(?string $number): int
    {
        if (null === $number) {
            throw new FibonacciException('Number not specified or incorrectly specified!');
        }

        $numberFibonacci = $this->fibonacci->run((int) $number);

        if (0 !== (int) $number && 0 === $numberFibonacci) {
            throw new FibonacciException('Number (' . $number . ') too large!');
        }

        return $numberFibonacci;
    }
}